# Delivery Service

Training project for microservices course

## Services shcema:

![alt text](schema/Delivery Service diagram.png)

## Services description:

1. Order - Processes orders from customers
2. Payment - Processes payment for orders 
3. Courier - CRUD courier information( e.g. location, current status) and choosing a courier on receipt order.

## User stories

- As a CUSTOMER I want to order a delivery
- As a CUSTOMER I want to pay for the delivery
- As a CUSTOMER I want to add delivery info for my order
- As a CUSTOMER I want to submit my order and pay for the delivery
- As a CUSTOMER I want to track my order status
- As a COURIER I want to be notified about a new order
- As a COURIER I want to confirm or reject the order
- As a COURIER I want to update the delivery information
